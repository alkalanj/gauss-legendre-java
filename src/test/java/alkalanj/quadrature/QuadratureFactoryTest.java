package alkalanj.quadrature;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class QuadratureFactoryTest {
	
	private static final double PRECISION = 1.0e-6;
	
	private static final Double[][] COEFS = {
		{  0.5555555555555556, 0.8888888888888888, 0.5555555555555556 },
		{ -0.7745966692414834, 0.0000000000000000, 0.7745966692414834 }
	};
	
	private static class CachedParams {
		public List<Double> weights;
		public List<Double> abscissas;
	}

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	@Test
	public void shouldWriteToCacheFile() throws IOException {
		File cacheDir = folder.newFolder("temp-cache");
		
		GauLegQuadrature q = QuadratureFactory.createCachedGauLeg(3, cacheDir.getPath());
		File cacheFile = cacheDir.toPath().resolve("3.json").toFile();
		
		assertTrue(cacheFile.exists());

		ObjectMapper mapper = new ObjectMapper();
		CachedParams params = mapper.readValue(cacheFile, CachedParams.class);
		
		for (int i = 0; i < 2; i++) {
			assertEquals(q.getWeights().get(i), params.weights.get(i), PRECISION);
			assertEquals(q.getAbscissas().get(i), params.abscissas.get(i), PRECISION);
		}
	}
	
	@Test
	public void shouldReadFromCacheFile() throws IOException {
		File cacheDir = folder.newFolder("temp-cache");
		File cacheFile = cacheDir.toPath().resolve("3.json").toFile();
		
		CachedParams params = new CachedParams();
		params.weights   = Arrays.asList(COEFS[0]);
		params.abscissas = Arrays.asList(COEFS[1]);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(cacheFile, params);
		
		GauLegQuadrature q = QuadratureFactory.createCachedGauLeg(3, cacheDir.getPath());
		
		for (int i = 0; i < 2; i++) {
			assertEquals(params.weights.get(i), q.getWeights().get(i), PRECISION);
			assertEquals(params.abscissas.get(i), q.getAbscissas().get(i), PRECISION);
		}
	}
	
}
