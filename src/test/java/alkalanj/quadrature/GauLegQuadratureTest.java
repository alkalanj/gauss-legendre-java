package alkalanj.quadrature;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class GauLegQuadratureTest {
	
	private static final double PRECISION = 1.0e-6;
	
	private static final Double[][][] COEFS = {
		{
			{  1.0000000000000000, 1.0000000000000000 },
			{ -0.5773502691896257, 0.5773502691896257 }
		},
		{
			{  0.5555555555555556, 0.8888888888888888, 0.5555555555555556 },
			{ -0.7745966692414834, 0.0000000000000000, 0.7745966692414834 }
		}
	};
	
	Function<Double, Double> linearF = new Function<Double, Double>() {
		public Double apply(Double x) {
			return x;
		}
	};
	
	Function<Double, Double> sinF = new Function<Double, Double>() {
		public Double apply(Double x) {
			return Math.sin(x);
		}
	};

	@Test
	public void testWithPrecalcCoefs() {
		List<Double> weights   = Arrays.asList(COEFS[0][0]);
		List<Double> abscissas = Arrays.asList(COEFS[0][1]);
		Quadrature q = new GauLegQuadrature(2, weights, abscissas);

		assertLinearCalc(q);
	}

	@Test
	public void testCalcCoefs2Points() {
		GauLegQuadrature q = new GauLegQuadrature(2);
		List<Double> weights = q.getWeights();
		List<Double> abscissas = q.getAbscissas();

		for (int i = 0; i < 1; i++) {
			assertEquals(COEFS[0][0][i], weights.get(i), PRECISION);
			assertEquals(COEFS[0][1][i], abscissas.get(i), PRECISION);
		}
		assertLinearCalc(q);
	}

	@Test
	public void testCalcCoefs3Points() {
		GauLegQuadrature q = new GauLegQuadrature(3);
		List<Double> weights = q.getWeights();
		List<Double> abscissas = q.getAbscissas();
		
		for (int i = 0; i < 2; i++) {
			assertEquals(COEFS[1][0][i], weights.get(i), PRECISION);
			assertEquals(COEFS[1][1][i], abscissas.get(i), PRECISION);
		}
		assertLinearCalc(q);
	}

	@Test
	public void testCalcCoefs5Points() {
		GauLegQuadrature q = new GauLegQuadrature(5);
		assertSineCalc(q);
	}
	
	private void assertLinearCalc(Quadrature q) {
		Double result = q.calculate(linearF, 0, 2);
		assertEquals(2.0, result, PRECISION);
	}
	
	private void assertSineCalc(Quadrature q) {
		Double result = q.calculate(sinF, 0, Math.PI);
		assertEquals(2.0, result, PRECISION);
	}
	
}
