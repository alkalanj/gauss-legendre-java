package alkalanj.quadrature;

import java.util.function.Function;

public interface Quadrature {

	public abstract double calculate(Function<Double, Double> f, double a,
			double b);

}
