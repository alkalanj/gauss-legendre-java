package alkalanj.quadrature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class GauLegQuadrature implements Quadrature {

	private Integer pointsNum;
	private List<Double> weights;
	private List<Double> abscissas;

	// Relative precision for calculating roots:
	private static final double EPS = 1.0e-14;

	public GauLegQuadrature(Integer pointsNum) {
		this.pointsNum = pointsNum;
		this.weights = new ArrayList<Double>(Collections.nCopies(this.pointsNum, 0.0));
		this.abscissas = new ArrayList<Double>(Collections.nCopies(this.pointsNum, 0.0));
		prepare();
	}

	public GauLegQuadrature(Integer pointsNum, List<Double> weights,
			List<Double> abscissas) {
		this.pointsNum = pointsNum;
		this.weights = weights;
		this.abscissas = abscissas;
	}

	private void prepare() {
		// Roots are symmetrical, only a half needs to be calculated:
		int m = (pointsNum + 1) / 2;
		for (int i = 0; i < m; i++) {
			// Initial guess for a root:
			double z = Math.cos(Math.PI * (i + 0.75) / (pointsNum + 0.5));
			double z1 = 0;
			double pp;
			do {
				double p1 = 1;
				double p2 = 0;
				// Recurrence relation to calculate Legendre's polynomial at z
				for (int j = 0; j < pointsNum; j++) {
					double temp = ((2 * j + 1) * z * p1 - j * p2) / (j + 1);
					p2 = p1;
					p1 = temp;
				}
				// Polynomial's derivative
				pp = pointsNum * (z * p1 - p2) / (z * z - 1);
				z1 = z;
				z = z1 - p1 / pp;	// Newton's method
			} while (Math.abs(z - z1) > EPS);

			abscissas.set(i, -z);
			abscissas.set(pointsNum - 1 - i, z);

			weights.set(i, 2 / ((1 - z * z) * pp * pp));
			weights.set(pointsNum - 1 - i, weights.get(i));
		}
	}

	/* (non-Javadoc)
	 * @see alkalanj.gausslegendre.Quadrature#calculate(java.util.function.Function, double, double)
	 */
	public double calculate(Function<Double, Double> f, double a, double b) {
		double c1 = (b - a) / 2;
		double c2 = (b + a) / 2;
		double sum = 0;
		for (int i = 0; i < pointsNum; i++) {
			sum += weights.get(i) * f.apply(c1 * abscissas.get(i) + c2);
		}
		return c1 * sum;
	}

	public Integer getPointsNum() {
		return pointsNum;
	}

	public void setPointsNum(Integer pointsNum) {
		this.pointsNum = pointsNum;
	}

	public List<Double> getWeights() {
		return weights;
	}

	public void setWeights(List<Double> weights) {
		this.weights = weights;
	}

	public List<Double> getAbscissas() {
		return abscissas;
	}

	public void setAbscissas(List<Double> abscissas) {
		this.abscissas = abscissas;
	}
	
}
