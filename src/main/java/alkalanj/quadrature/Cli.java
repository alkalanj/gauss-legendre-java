package alkalanj.quadrature;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class Cli {

	static final String OPTION_A = "a";
	static final String OPTION_B = "b";
	static final String OPTION_N = "n";
	static final String OPTION_HELP = "h";
	
	static final String DEFAULT_A = "-1.00";
	static final String DEFAULT_B = "1.00";
	static final String DEFAULT_N = "5";

	static final String cacheDir = System.getProperty("user.home") + File.separator + ".gauss-legendre-quad";
	
	Options options = new Options();
	CommandLineParser parser = new DefaultParser();
	HelpFormatter formatter = new HelpFormatter();
	
	public Cli() {
		options.addOption(OPTION_A,    "from",   true,  "lower boundary of the integration interval (expression)");
		options.addOption(OPTION_B,    "to",     true,  "upper boundary of the integration interval (expression)");
		options.addOption(OPTION_N,    "points", true,  "number of integration points (integer)");
		options.addOption(OPTION_HELP, "help",   false, "Print this message");
	}
	
	public void run(String[] args) throws JsonParseException, JsonMappingException, IOException {
		CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
			
			if (cmd.hasOption(OPTION_HELP)) {
				help();
			} else {
			
				Double a = ExpressionFunction.getValue(cmd.getOptionValue(OPTION_A, DEFAULT_A));
				Double b = ExpressionFunction.getValue(cmd.getOptionValue(OPTION_B, DEFAULT_B));
				
				Integer n = Integer.parseUnsignedInt(cmd.getOptionValue(OPTION_N, DEFAULT_N));
				
				if (cmd.getArgList().size() >= 1) {
					String functionExpr = cmd.getArgList().get(0);
					
					Function<Double, Double> f = new ExpressionFunction(functionExpr);
					
					Quadrature quad = QuadratureFactory.createCachedGauLeg(n, cacheDir);
					Double result = quad.calculate(f, a, b);
					
					System.out.println("Result: " + result);
				} else {
					System.out.println("You must provide function expression.");
				}

			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println("Wrong number format.");
		}
	}
	
	private void help() {
		formatter.printHelp("java -jar quad.jar [OPTIONS] <FUNCTION_EXPR>", options);
	}
	
}
