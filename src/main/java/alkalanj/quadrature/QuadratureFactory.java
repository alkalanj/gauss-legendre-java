package alkalanj.quadrature;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class QuadratureFactory {

	private static class CachedParams {
		public List<Double> weights;
		public List<Double> abscissas;
	}
	
	public static GauLegQuadrature createCachedGauLeg(Integer pointsNum, String cacheDirPath)
			throws JsonParseException, JsonMappingException, IOException {
		File cacheDir = new File(cacheDirPath);
		cacheDir.mkdirs();
		
		ObjectMapper mapper = new ObjectMapper();
		GauLegQuadrature quad;
		
		File cacheFile = cacheDir.toPath().resolve(pointsNum + ".json").toFile();
		if (cacheFile.isFile()) {
			CachedParams params = mapper.readValue(cacheFile, CachedParams.class);
			quad = new GauLegQuadrature(pointsNum, params.weights, params.abscissas);
		} else {
			quad = new GauLegQuadrature(pointsNum);
			CachedParams params = new CachedParams();
			params.weights = quad.getWeights();
			params.abscissas = quad.getAbscissas();
			mapper.writeValue(cacheFile, params);
		}
		
		return quad;
	}

}
