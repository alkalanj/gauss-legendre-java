package alkalanj.quadrature;

import java.util.function.Function;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class ExpressionFunction implements Function<Double, Double> {

	private static final String VARIABLE_LABEL = "x";

	Expression expression;

	public ExpressionFunction(String functionExpr) {
		this.expression = new ExpressionBuilder(functionExpr).variable(
				VARIABLE_LABEL).build();
	}

	public Double apply(Double x) {
		expression.setVariable(VARIABLE_LABEL, x);
		return expression.evaluate();
	}

	public static Double getValue(String expr) {
		return new ExpressionBuilder(expr).build().evaluate();
	}
	
}
